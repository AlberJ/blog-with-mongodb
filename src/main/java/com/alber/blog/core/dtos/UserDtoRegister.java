package com.alber.blog.core.dtos;

import com.alber.blog.core.models.ClientUser;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class UserDtoRegister {
    private String name;
    private String email;

    public ClientUser parseToClientUser() {
        return ClientUser.builder()
                .name(this.name)
                .email(this.email)
                .createdAt(LocalDateTime.now())
                .build();
    }
}
