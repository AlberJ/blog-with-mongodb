package com.alber.blog.core.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import org.springframework.data.annotation.Id;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Builder(builderMethodName = "builder")
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "user")
public class ClientUser {

    @Email(message = "invalid email format")
    @Id
    private String email;

    @Size(min = 5, message = "must be at least 5 characters")
    private String name;

    private LocalDateTime createdAt;
}
