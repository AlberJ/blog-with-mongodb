package com.alber.blog.core.services;

import com.alber.blog.core.models.ClientUser;
import com.alber.blog.core.repositories.UserRepository;
import com.alber.blog.settings.exceptions.BadRequestException;
import com.alber.blog.settings.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public void add(ClientUser clientUser) {
        try {
            repository.save(clientUser);
        } catch (RuntimeException exception) {
            throw new BadRequestException(exception.getMessage());
        }
    }

    public ClientUser getByEmail(String email) {
        return repository.findByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));
    }
}
