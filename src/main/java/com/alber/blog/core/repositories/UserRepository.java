package com.alber.blog.core.repositories;

import com.alber.blog.core.models.ClientUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<ClientUser, String> {
    Optional<ClientUser> findByEmail(String email);
}
